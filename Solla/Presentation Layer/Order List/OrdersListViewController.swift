//
//  OrdersListViewController.swift
//  Solla
//
//  Created by arabpas on 11/8/20.
//

import UIKit
import RxCocoa
import RxSwift

class OrdersListViewController: BaseViewController,UITableViewDelegate {

    let bag = DisposeBag()
    @IBOutlet weak var ordersTableView: UITableView!
    var orders: BehaviorSubject<[Order]> = BehaviorSubject(value: [])
    private let network = NetworkModel<UserEndPoint>()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupaddressTableView()
        getOrderss()
        
    }
    
    func getOrderss() {
        let d:Observable<OrdersModel> = network.fetch(request: .orders)
            d.subscribe (onNext: { (data) in
                self.orders.onNext(data.orders ?? [])
        }, onError: { (error) in
            print(error.localizedDescription)
        }).disposed(by: bag)

    }
    
    func setupaddressTableView() {
        ordersTableView.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        ordersTableView.delegate = self
        orders
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(ordersTableView.rx.items(cellIdentifier: "cell", cellType: OrderTableViewCell.self))
            {row,element,cell in
                cell.setupCell(order: element)
            }.disposed(by: bag)
    }

}
