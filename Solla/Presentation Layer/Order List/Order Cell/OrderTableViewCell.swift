//
//  OrderTableViewCell.swift
//  Solla
//
//  Created by arabpas on 11/8/20.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var orderNumLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(order: Order) {
        orderNumLabel.text = order.orderNumber ?? ""
    }
    
}
