//
//  ProductDetailsViewModel.swift
//  Solla
//
//  Created by arabpas on 11/6/20.
//

import Foundation
import RxSwift
import RxCocoa


class ProductDetailsViewModel:BaseViewModel {
    
    var bag = DisposeBag()
    weak var view: ProductDetailsViewController?
    
    var product: Product?
    private let network = NetworkModel<UserEndPoint>()
    
    init(view: ProductDetailsViewController,product:Product?) {
        super.init(viewDelegate: view)
        self.view = view
        self.product = product
    }
    
}

extension ProductDetailsViewModel {
    
}
