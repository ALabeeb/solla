//
//  AddressListViewController.swift
//  Solla
//
//  Created by arabpas on 11/7/20.
//

import UIKit
import RxSwift
import RxCocoa

class AddressListViewController: BaseViewController, UITableViewDelegate {
    
    let bag = DisposeBag()
    @IBOutlet weak var addressTableView: UITableView!
    var addresses: BehaviorSubject<[Address]> = BehaviorSubject(value: [])
    private let network = NetworkModel<UserEndPoint>()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupaddressTableView()
        getAddresses()
        
    }
    
    func getAddresses() {
        let d:Observable<AddressModel> = network.fetch(request: .addresses)
            d.subscribe (onNext: { (data) in
                self.addresses.onNext(data.addresses ?? [])
        }, onError: { (error) in
            print(error.localizedDescription)
        }).disposed(by: bag)

    }
    
    func setupaddressTableView() {
        addressTableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        addressTableView.delegate = self
        addresses
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(addressTableView.rx.items(cellIdentifier: "cell", cellType: AddressTableViewCell.self))
            {row,element,cell in
                cell.setupCell(address: element)
            }.disposed(by: bag)
    }
    @IBAction func addAddressTapped(_ sender: Any) {
    }
    
}
