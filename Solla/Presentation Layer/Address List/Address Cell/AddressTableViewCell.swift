//
//  AddressTableViewCell.swift
//  Solla
//
//  Created by arabpas on 11/7/20.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupCell(address: Address) {
        nameLabel.text = address.name ?? ""
    }

    
}
