//
//  BasePresenter.swift
//  testNavigation
//
//  Created by Ahmed Osman on 4/16/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewModel {
    
    weak var viewDelegate: BaseViewDelegate?
    
    init(viewDelegate: BaseViewDelegate?){
        self.viewDelegate = viewDelegate
    }
    
    func handleError(error: Error) {
        viewDelegate?.hideLoader()
        let nserror = error as NSError
        guard let message = nserror.userInfo["message"] as? String else {
            return
        }
        viewDelegate?.showErrorState(message: message)
    }
            
//    func uploadImage(image: UIImage) -> Observable<String> {
//        let normalizedImage = image.normalizeImage()
//        let imagesUploader = NetworkInteractor<ImagesEndPoint>()
//        return imagesUploader.upload(request: .uploadImage(images: [normalizedImage])).map(ImageResponse.self).asObservable().map({
//            return $0.data.first!.id
//        })
//    }

}
