//
//  BaseViewController.swift
//  testNavigation
//
//  Created by Ahmed Osman on 4/16/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import UIKit


protocol BaseViewDelegate: class {
    func showLoader()
    func hideLoader()
    func showEmptyState(message: String)
    func hideEmptyState()
    func showErrorState(message: String)
    func hideErrorState()
    func showAlert(message: String, completion: @escaping ()->Void)
    func showConfirmationAlert(message: String, completion: @escaping ()->Void)
    func showUpdateAlert()
}

class BaseViewController: UIViewController, BaseViewDelegate {
//    private var loadingAlert:Alert?
//    private var emptyAlert:Alert?
    var errorAlert:UIAlertController?
    let statusBarView = UIView()
    lazy var backgroundImageView: UIImageView = {
        let backgroundImageView = UIImageView()
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(backgroundImageView)
        self.view.sendSubviewToBack(backgroundImageView)
        NSLayoutConstraint.activate([backgroundImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor), backgroundImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor), backgroundImageView.topAnchor.constraint(equalTo: self.view.topAnchor), NSLayoutConstraint(item: backgroundImageView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 0.5, constant: 0)])
        return backgroundImageView
    }()
    let indicator: UIActivityIndicatorView = {
        let ind = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        ind.style = UIActivityIndicatorView.Style.gray
        ind.hidesWhenStopped = true
        return ind
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        changeStatusBarBackground()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        DispatchQueue.main.async {
            self.changeStatusBarBackground()
            let x = self.view.frame.width/2
            let y = self.view.frame.height/2
            self.indicator.center = CGPoint(x: x, y: y)
        }
    }
    
    func changeStatusBarBackground() {
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBarColor = UIColor(red: 0/255, green: 89/255, blue: 77/255, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        view.bringSubviewToFront(statusBarView)
        statusBarView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint(item: statusBarView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0), NSLayoutConstraint(item: statusBarView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0), NSLayoutConstraint(item: statusBarView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0), statusBarView.heightAnchor.constraint(equalToConstant: UIApplication.shared.statusBarFrame.height)])
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor(red: 27/255, green: 27/255, blue: 78/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
    }
    
    func showErrorState(message: String){
        let action = UIAlertAction(title: "", style: .cancel, handler: nil)
        errorAlert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: message, preferredStyle: .alert)
        errorAlert?.addAction(action)
        self.present(errorAlert!, animated: true, completion: nil)
    }
    
    func setupBackButton() {
        let backButton = UIImage(named: "Icon-Back")
        self.navigationController?.navigationBar.backIndicatorImage = backButton
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backButton
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func showLoader() {
            self.view.addSubview(indicator)
    //        let x = self.view.frame.width/2
    //        let y = self.view.frame.height/2
    //        indicator.center = CGPoint(x: x, y: y)
            UIApplication.shared.beginIgnoringInteractionEvents()
            indicator.translatesAutoresizingMaskIntoConstraints = false
            indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            self.view.bringSubviewToFront(indicator)
            indicator.startAnimating()
        }
        
        func hideLoader() {
            UIApplication.shared.endIgnoringInteractionEvents()
            indicator.stopAnimating()
        }
}

extension BaseViewController {
    func showEmptyState(message: String){
//        let emptyView = UIView()
//        self.emptyAlert = Alert(ownerView: .custom(view: self.view), dimViewStyle:.custom(color: UIColor.black, alpha: 0.3))
//        errorAlert?.backgroundColor = nil
//        errorAlert?.setContentView(view: emptyView)
//        errorAlert?.present()
    }
    
    func hideEmptyState(){
//        self.emptyAlert?.dismiss()
    }
    
    func showAlert(message: String, completion: @escaping ()->Void){
        let action = UIAlertAction(title: "", style: .cancel) { (alertAction) in
            completion()
        }
        let errorAlert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        errorAlert.addAction(action)
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    func showConfirmationAlert(message: String, completion: @escaping ()->Void){
        let cancelAction = UIAlertAction(title: "", style: .cancel)
        let okAction = UIAlertAction(title: "", style: .default, handler: { (event) in
            completion()
        })
        let errorAlert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        errorAlert.addAction(cancelAction)
        errorAlert.addAction(okAction)
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    func showUpdateAlert() {
        self.showConfirmationAlert(message: "New update avilable", completion: {
            if let url = URL(string: "https://itunes.apple.com/app/id1480594065?mt=8") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })
    }
}

extension BaseViewController {

    func hideErrorState(){
        errorAlert?.dismiss(animated: true, completion: nil)
    }
}

extension BaseViewController {
    
    func hideBackButton() {
        self.navigationItem.hidesBackButton = true
    }
    
    func showBackButton() {
        if let views = navigationController?.viewControllers, views.count > 1 {
            self.navigationItem.hidesBackButton = false
        }
    }
    
    func hideNavigationBar() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func showNavigationBar() {
        self.navigationController?.navigationBar.isHidden = false
    }
}
