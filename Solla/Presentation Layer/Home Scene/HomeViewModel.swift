//
//  HomeViewModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/3/20.
//

import Foundation

import Foundation
import RxSwift
import RxCocoa
import Moya

class HomeViewModel: BaseViewModel {
    
    var bag: DisposeBag = DisposeBag()
    var view: HomeViewController?
    
    var products: BehaviorSubject<[Product]> = BehaviorSubject(value: [])
    var offers: BehaviorSubject<[Offer]> = BehaviorSubject(value: [])
    var brands: BehaviorSubject<[Brand]> = BehaviorSubject(value: [])
    var categories: BehaviorSubject<[Category]> = BehaviorSubject(value: [])
    var advs: BehaviorSubject<[Adv]> = BehaviorSubject(value: [])
    
    init(view: HomeViewController) {
        super.init(viewDelegate: view)
        self.view = view
    }
    private let network = NetworkModel<HomeEndPoint>()
}

extension HomeViewModel {
    
    func getOffers(){
        let dd: Observable<OffersModel> = network.fetch(request: .offers)
        dd.subscribe (onNext: { (model) in
            let data = model.offers ?? []
            self.offers.onNext(data)
        }).disposed(by: bag)
    }
    
    func getAdss(){
        let dd: Observable<AdvsModel> = network.fetch(request: .advs)
        dd.subscribe (onNext: { (model) in
            let data = model.Advs ?? []
            self.advs.onNext(data)
        }).disposed(by: bag)
    }
    
    func getBrands(){
        let dd: Observable<BrandsModel> = network.fetch(request: .allBrands)
        dd.subscribe (onNext: { (model) in
            let data = model.brands ?? []
            self.brands.onNext(data)
        }).disposed(by: bag)
    }
    func getCategories(){
        let dd: Observable<CategoriesModel> = network.fetch(request: .allCategories)
        dd.subscribe (onNext: { (model) in
            let data = model.categories ?? []
            self.categories.onNext(data)
        }).disposed(by: bag)
    }
    
}



