//
//  ProductCollectionViewCell.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/2/20.
//

import UIKit
import MaterialComponents

class ProductCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize.init(width: 150, height: 200)
    
    @IBOutlet weak var containerView: MDCCard!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pastPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.cornerRadius = 8
    }
    
    func setUpCell(product:Product) {
        nameLabel.text = product.title ?? ""
        pointsLabel.text = product.points ?? ""
        priceLabel.text = product.price ?? ""
        pastPriceLabel.text = ""
        setImage(url: product.images?.first?.image)
    }
    
    func setUpCellOffer(offer:Offer) {
        nameLabel.text = offer.product?.title ?? ""
        pointsLabel.text = offer.product?.points ?? ""
        priceLabel.text = offer.price_after ?? ""
        pastPriceLabel.text = offer.price_before ?? ""
        setImage(url: offer.image)
    }
    
    func setImage(url: String?) {
        if let url = URL(string: url ?? "") {
            productImage.kf.setImage(with: url)
        }
    }
    

}
