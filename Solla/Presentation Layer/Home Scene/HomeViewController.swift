//
//  HomeViewController.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/1/20.
//

import UIKit
import RxCocoa
import RxSwift
import MaterialComponents


class HomeViewController: BaseViewController, UICollectionViewDelegate {
    
    //MARK:- Private variables
     private let bag = DisposeBag()
    
    //MARK:- Collection Outlets
    @IBOutlet weak var adsCollectionView: UICollectionView!
    @IBOutlet weak var brandsCollectionView: UICollectionView!
    @IBOutlet weak var offersCollectionView: UICollectionView!
    @IBOutlet weak var allCollectionView: UICollectionView!
    @IBOutlet weak var bestCollectionView: UICollectionView!
    
    var homeViewModel: HomeViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLayout()
        self.setupOffersCollectionView()
        self.setupOffersCollectionViewDidSelect()
        self.setupAdvsCollectionView()
        self.setupBrandsCollectionView()
        homeViewModel?.getOffers()
        homeViewModel?.getAdss()
        homeViewModel?.getBrands()
    }
    
    func setUpLayout() {
        let layout = offersCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.minimumLineSpacing = 2
    }
    
    func setupAdvsCollectionView() {
        adsCollectionView.register(UINib.init(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        adsCollectionView.delegate = self
        homeViewModel?.advs
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(adsCollectionView.rx.items(cellIdentifier: "cell",cellType: AdsCollectionViewCell.self)) { row,elment,cell in
                cell.setupCell(image: elment.image ?? "")
            }.disposed(by: bag)
        
    }
    
    func setupBrandsCollectionView() {
        brandsCollectionView.register(UINib.init(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        brandsCollectionView.delegate = self
        homeViewModel?.brands
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(brandsCollectionView.rx.items(cellIdentifier: "cell",cellType: AdsCollectionViewCell.self)) { row,elment,cell in
                cell.setupCell(image: elment.image ?? "")
            }.disposed(by: bag)
        
    }
    
    func setupOffersCollectionView() {
        offersCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        offersCollectionView.delegate = self
        homeViewModel?.offers.observeOn(MainScheduler.instance).asDriver(onErrorJustReturn: [])
            .drive(offersCollectionView.rx.items(cellIdentifier: "cell", cellType: ProductCollectionViewCell.self)) {row,elment,cell in
                cell.setUpCellOffer(offer: elment)
            }.disposed(by: bag)
            
    }
    
    func setupOffersCollectionViewDidSelect() {
        offersCollectionView.rx.modelSelected(Offer.self).subscribe (onNext: { (offer) in
            self.navigateToProductDetails(product: offer.product)
        }).disposed(by: bag)

    }
    
    @IBAction func showAllBrands(_ sender: Any) {
    }
    @IBAction func showAllOffers(_ sender: Any) {
    }
    @IBAction func showAllCategories(_ sender: Any) {
    }
    
    func navigateToProductDetails(product: Product?) {
        let productVC = ProductDetailsViewController.init(nibName:"ProductDetailsViewController", bundle: nil)
        let model = ProductDetailsViewModel.init(view: productVC,product: product)
        productVC.viewModel = model
        self.navigationController?.pushViewController(productVC, animated: true)
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case offersCollectionView,bestCollectionView:
            return ProductCollectionViewCell.size
        case adsCollectionView:
            return CGSize.init(width: 198, height: 93)
        case allCollectionView:
            return CGSize.init(width: 150, height: 150)
        case brandsCollectionView:
            return CGSize.init(width: 100, height: 50)
        default:
            return .zero
        }
    }
    
}
