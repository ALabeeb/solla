//
//  AdsCollectionViewCell.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import UIKit
import Kingfisher

class AdsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(image: String, title: String? = "") {
        titleLabel.text = title
        if let url = URL(string: image){
            self.image.kf.setImage(with: url)
        }
        
    }

}
