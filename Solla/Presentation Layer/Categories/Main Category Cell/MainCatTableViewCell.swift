//
//  MainCatTableViewCell.swift
//  Solla
//
//  Created by arabpas on 11/6/20.
//

import UIKit
import RxCocoa
import RxSwift

class MainCatTableViewCell: UITableViewCell, UICollectionViewDelegate {
    

    let bag = DisposeBag()
    @IBOutlet weak var catName: UILabel!
    var products: BehaviorSubject<[Product]> = BehaviorSubject.init(value: [])
    @IBOutlet weak var productsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        productsCollectionView.delegate = self
    }
    
    func setUpCell(catName: String, products: [Product]) {
        self.products.onNext(products)
        self.catName.text = catName
    }
    
    func setUpProductsCollectionView() {
        productsCollectionView.register(UINib.init(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        productsCollectionView.delegate = self
        products
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(productsCollectionView.rx.items(cellIdentifier: "cell",cellType: AdsCollectionViewCell.self)) { row,elment,cell in
                cell.setupCell(image: elment.images?.first?.image ?? "",title: "")
            }.disposed(by: bag)
    }
    
    func setUpProductsCollectionViewDidSelect() {
        
    }

    @IBAction func showAllTapped(_ sender: Any) {
    }
    
}


