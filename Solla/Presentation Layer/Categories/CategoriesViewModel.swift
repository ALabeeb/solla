//
//  CategoriesViewModel.swift
//  Solla
//
//  Created by arabpas on 11/6/20.
//

import Foundation
import RxSwift
import RxCocoa


class CategoriesViewModel: BaseViewModel {
    var bag: DisposeBag = DisposeBag()
    var view: CategoriesViewController?
    var categories: BehaviorSubject<[Category]> = BehaviorSubject(value: [])
    private let network = NetworkModel<HomeEndPoint>()
    
    init(view: CategoriesViewController) {
        super.init(viewDelegate: view)
        self.view = view
    }
}

extension CategoriesViewModel {
    
    func getCategories(){
        let dd: Observable<CategoriesModel> = network.fetch(request: .allCategories)
        dd.subscribe (onNext: { (model) in
            let data = model.categories ?? []
            self.categories.onNext(data)
        }).disposed(by: bag)
    }
    
}



