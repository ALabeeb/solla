//
//  CategoriesViewController.swift
//  Solla
//
//  Created by arabpas on 11/6/20.
//

import UIKit
import RxSwift
import RxCocoa

class CategoriesViewController: BaseViewController,UICollectionViewDelegate {
    
    let bag = DisposeBag()
    @IBOutlet weak var allCatCollectionView: UICollectionView!
    
    @IBOutlet weak var categoryBanner: UIImageView!
    @IBOutlet weak var allProductstableView: UITableView!
    var viewModel: CategoriesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    func setUpAllCatCollectionView() {
        allCatCollectionView.register(UINib.init(nibName: "AdsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        allCatCollectionView.delegate = self
        viewModel?.categories
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(allCatCollectionView.rx.items(cellIdentifier: "cell",cellType: AdsCollectionViewCell.self)) { row,elment,cell in
                cell.setupCell(image: elment.image ?? "",title: "")
            }.disposed(by: bag)
    }
}
