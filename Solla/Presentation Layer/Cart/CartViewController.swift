//
//  CartViewController.swift
//  Solla
//
//  Created by arabpas on 11/8/20.
//

import UIKit
import RxCocoa
import RxSwift

class CartViewController: BaseViewController,UITableViewDelegate {

    let bag = DisposeBag()
    @IBOutlet weak var productsTableView: UITableView!
    var products: BehaviorSubject<[Product]> = BehaviorSubject(value: [])
    private let network = NetworkModel<UserEndPoint>()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupaddressTableView()
        getProducts()
        
    }
    
    func getProducts() {
        let d:Observable<ProductsModel> = network.fetch(request: .cart)
            d.subscribe (onNext: { (data) in
                self.products.onNext(data.products ?? [])
        }, onError: { (error) in
            print(error.localizedDescription)
        }).disposed(by: bag)

    }
    
    func setupaddressTableView() {
        productsTableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        productsTableView.delegate = self
        products
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])
            .drive(productsTableView.rx.items(cellIdentifier: "cell", cellType: CartTableViewCell.self))
            {row,element,cell in
                cell.setUpCell(product: element)
            }.disposed(by: bag)
    }
    @IBAction func continueTapped(_ sender: Any) {
    }

}
