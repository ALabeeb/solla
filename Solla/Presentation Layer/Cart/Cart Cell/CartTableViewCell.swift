//
//  CartTableViewCell.swift
//  Solla
//
//  Created by arabpas on 11/8/20.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pastPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpCell(product:Product) {
        nameLabel.text = product.title ?? ""
        pointsLabel.text = product.points ?? ""
        priceLabel.text = product.price ?? ""
        pastPriceLabel.text = ""
        setImage(url: product.images?.first?.image)
    }
    
    func setImage(url: String?) {
        if let url = URL(string: url ?? "") {
            productImage.kf.setImage(with: url)
        }
    }
    
}
