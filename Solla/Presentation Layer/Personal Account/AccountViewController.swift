//
//  AccountViewController.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/1/20.
//

import UIKit

class AccountViewController: BaseViewController {
    
    
    @IBOutlet weak var userImage: UIImageView!

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userMailLabel: UILabel!
    @IBOutlet weak var userPointsLabel: UILabel!
    
    var viewModel: AccountViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad() 
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getUserData()
    }

    @IBAction func showOrdersTapped(_ sender: Any) {
    }
    

    @IBAction func showFavsTapped(_ sender: Any) {
    }
    @IBAction func showMembersTapped(_ sender: Any) {
    }
    @IBAction func showProfileTapped(_ sender: Any) {
    }
    @IBAction func showAddressesTapped(_ sender: Any) {
    }
    @IBAction func contactUsTapped(_ sender: Any) {
    }
    @IBAction func aboutUsTapped(_ sender: Any) {
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
    }
    

}
