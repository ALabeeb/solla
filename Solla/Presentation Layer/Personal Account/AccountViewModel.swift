//
//  AccountViewModel.swift
//  Solla
//
//  Created by arabpas on 11/5/20.
//

import Foundation
import RxCocoa
import RxSwift

class AccountViewModel:BaseViewModel {
    
    var bag = DisposeBag()
    weak var view: AccountViewController?
    
    var user: User?
    private let network = NetworkModel<UserEndPoint>()
    
    init(view: AccountViewController) {
        super.init(viewDelegate: view)
        self.view = view
    }
    
}

extension AccountViewModel {
    
    func getUserData() {
        self.view?.showLoader()
        let user: Observable<UserModel> = network.fetch(request: .userData)
        user.subscribe (onNext: { (model) in
            self.view?.hideLoader()
            let user = model.user
            self.view?.userNameLabel.text = "\(user?.firstname ?? "") \(user?.lastname ?? "")"
            self.view?.userMailLabel.text = user?.email ?? ""
            self.view?.userPointsLabel.text = user?.status ?? ""
        }).disposed(by: bag)
    }
    
}
