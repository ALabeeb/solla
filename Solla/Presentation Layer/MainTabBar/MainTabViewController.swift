//
//  MainTabViewController.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/4/20.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabView()
        setViewControllers()
        
    }
    
    func setViewControllers() {
        self.viewControllers = [getHomeNav(),getCategoriesNav(),getUserNav()]
    }
    
    func setupTabView() {
        self.tabBar.tintColor = #colorLiteral(red: 0.5685598254, green: 0.2964760959, blue: 0.639883101, alpha: 1)
    }
    
    func getHomeNav() -> UINavigationController {
        
        let mainVC = HomeViewController.init(nibName: "HomeViewController", bundle: nil)
        let homeViewModel = HomeViewModel.init(view: mainVC)
        mainVC.homeViewModel = homeViewModel
        let mainNav = UINavigationController.init(rootViewController: mainVC)
        let mainIcon = UITabBarItem.init(title: "Main".localized, image: #imageLiteral(resourceName: "home-tab"), tag: 0)
        mainVC.tabBarItem = mainIcon
        mainNav.navigationBar.backgroundColor = .gray
        return mainNav
    }
    
    func getUserNav() -> UINavigationController {
        let userVC = AccountViewController.init(nibName: "AccountViewController", bundle: nil)
        let userNav = UINavigationController.init(rootViewController: userVC)
        let userViewModel = AccountViewModel.init(view: userVC)
        userVC.viewModel = userViewModel
        let userIcon = UITabBarItem.init(title: "User".localized, image: #imageLiteral(resourceName: "user-tab"), tag: 0)
        userVC.tabBarItem = userIcon
        userNav.navigationBar.backgroundColor = .gray
        return userNav
    }
    
    func getCategoriesNav() -> UINavigationController {
        let vc = CategoriesViewController.init(nibName: "CategoriesViewController", bundle: nil)
        let nav = UINavigationController.init(rootViewController: vc)
        let viewModel = CategoriesViewModel.init(view: vc)
        vc.viewModel = viewModel
        let icon = UITabBarItem.init(title: "Categories".localized, image: #imageLiteral(resourceName: "types-tab"), tag: 0)
        vc.tabBarItem = icon
        nav.navigationBar.backgroundColor = .gray
        return nav
    }
    
    func getCartNav() -> UINavigationController {
        let vc = CategoriesViewController.init(nibName: "CategoriesViewController", bundle: nil)
        let nav = UINavigationController.init(rootViewController: vc)
        let viewModel = CategoriesViewModel.init(view: vc)
        vc.viewModel = viewModel
        let icon = UITabBarItem.init(title: "Categories".localized, image: #imageLiteral(resourceName: "types-tab"), tag: 0)
        vc.tabBarItem = icon
        nav.navigationBar.backgroundColor = .gray
        return nav
    }

    

}

extension String {
    var localized: String {
        get {return NSLocalizedString(self, comment: "")}
    }
}
