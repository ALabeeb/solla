//
//  K.swift
//  dawar
//
//  Created by Ahmed Osman on 7/15/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation

struct K {
    struct Auth {
        static let identifierKey = "identifier"
        static let passwordKey = "password"
        static let tokenKey = "token"
    }
    
    struct User {
        static let userType = "userType"
    }
    
    struct LoginIdentifiers {
        static let loginKey = "loginType"
        static let facebook = "facebook"
        static let google = "google"
        static let credentials = "credentials"
    }
    
    struct TestingEnviroment {
        struct Testing {
            static let requestCellIdentifier = "requestCellIdentifier"
            static let isStub = "isStub"
            static let stubURL = "stubURL"
            static let isCache = "isCache"
            static let savedPaths = "savedPaths"
        }
    }
    
    struct NetworkError {
        struct Error {
            static let defaultMessage = "An error occurred, please try again sometime."
            static let defaultTitle = "Maintenance error"
            static let defaultImage = ""
            static let defaultCode = -1
        }
        
        struct HTTPStatusErrorMessages {
            static let serverError = "Sorry, our service unavilable now. Please try again in a while."
            static let clientError = "Sorry, an error occured please reload the page."
        }
        
        struct HTTPStatusErrorTitles {
            static let serverError = "Service is not avilable"
            static let clientError = "An error occured"
        }
        
        struct HTTPStatusErrorImages {
            static let serverError = ""
            static let clientError = ""
        }
        
        struct NetworkErrorMessages {
            static let timeout = "Looks like the server is taking to long to respond"
            static let requestCancelled = "Sorry, An internal error occured"
            static let notConnectedToInternet = "Cannot connect to internet, please check your internet connection"
            static let userAuthenticationRequired = "Please, you must be logged in to complete access this service"
            static let resourceUnavailable = "Sorry, our service unavilable now. Please try again in a while"
        }
        
        struct NetworkErrorTitles {
            static let timeout = "Time out"
            static let requestCancelled = "Maintenance"
            static let notConnectedToInternet = "No internet connection"
            static let userAuthenticationRequired = "Authentication Required"
            static let resourceUnavailable = "Service unavilable"
        }
        
        struct NetworkErrorImages {
            static let timeout = "Time out"
            static let requestCancelled = "Maintenance"
            static let notConnectedToInternet = "No internet connection"
            static let userAuthenticationRequired = "Authentication Required"
            static let resourceUnavailable = "Service unavilable"
        }
    }
    
    struct ViewControllers {
        static let launchViewController = "LaunchViewController"
        static let loginViewController = "LoginViewController"
        static let passwordRecoveryViewController = "PasswordRecoveryViewController"
        static let forgotPasswordViewController = "ForgotPasswordViewController"
        static let passwordSent = "PasswordSentViewController"
        static let registerViewController = "RegisterViewController"
    }
    
    struct ErrorMessages {
        static let passwordMismatch = "Passwords mismatch"
        static let invalidPassword = "Password Length must be between 8 - 15 digits"
        static let loginPasswordInvalid = "Password Length must be between 8 - 15"
        static let invalidEmail = "You entered wrong email format"
        static let empty = "This field is required"
        static let invalidCredentials = "Invalid Credentials"
    }
    
    struct SuccessMessages {
        static let passwordChangedMessage = "Password changed successfully!"
    }
}
