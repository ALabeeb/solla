//
//  UserModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import Foundation

struct UserModel: Codable {
    let message: String?
    let token: String?
    let user: User?
    
    enum CodingKeys: String, CodingKey {
        case message, token, user = "data"
    }
}

struct User: Codable {
    let id: Int?
    let firstname, lastname, gender, phone: String?
    let email, status, code: String?

    enum CodingKeys: String, CodingKey {
        case id, firstname, lastname, gender, phone, email, status, code
    }
}
