//
//  BrandsModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import Foundation


struct BrandsModel: Codable {
    let message: String?
    let brands: [Brand]?
    
    enum CodingKeys: String, CodingKey {
        case message
        case brands = "data"
    }
}

struct Brand: Codable {
    let id: Int?
    let image: String?
    let name: String?
        
}
