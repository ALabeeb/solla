//
//  CategoriesModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import Foundation


struct CategoriesModel: Codable {
    let message: String?
    let categories: [Category]?
    
    enum CodingKeys: String, CodingKey {
        case message
        case categories = "data"
    }
}

struct Category: Codable {
    let id: Int?
    let image: String?
    let name: String?
    let subCategories: [Category]?
    
    enum CodingKeys: String, CodingKey {
        case id,name,image
        case subCategories = "sub_categories"
    }
}

