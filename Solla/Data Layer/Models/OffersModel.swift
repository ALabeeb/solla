//
//  OffersModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import Foundation

struct OffersModel: Codable {
    let message: String?
    let offers: [Offer]?
    let count: Int?
    
    enum CodingKeys: String, CodingKey {
        case message,count
        case offers = "data"
    }
    
}

struct Offer: Codable {
    let id: Int?
    let product_id: String?
    let image: String?
    let discount: String?
    let price_before: String?
    let price_after: String?
    let product: Product?
}
