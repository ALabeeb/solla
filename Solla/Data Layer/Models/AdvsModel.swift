//
//  AdsModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/5/20.
//

import Foundation


struct AdvsModel: Codable {
    let message: String?
    let Advs: [Adv]?
    
    enum CodingKeys: String, CodingKey {
        case message
        case Advs = "data"
    }
}

struct Adv: Codable {
    let id: Int?
    let image: String?
    let productId: String?
        
    enum CodingKeys: String, CodingKey {
        case id, image
        case productId = "product_id"
    }
}
