//
//  ProductModel.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/3/20.
//

import Foundation

struct ProductsModel: Codable {
    let message: String?
    let products: [Product]?
    let count: Int?
    
    enum CodingKeys: String, CodingKey {
        case message,count
        case products = "data"
    }
    
}

// MARK: - Datum
struct Product: Codable {
    let id: Int
    let title, bref, descrition, price: String?
    let points, quantity, supplier, guarantee: String?
    let policiesOfReturn, brandID, categoryID, subCategoryID: String?
    let createdAt: String?
    let updatedAt: String?
    let images: [ProductImages]?

    enum CodingKeys: String, CodingKey {
        case id, title, bref, descrition, price, points, quantity, supplier, guarantee
        case policiesOfReturn = "policies_of_return"
        case brandID = "brand_id"
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case images
    }
}

struct ProductImages:Codable {
    let image: String?
}
