//
//  Logger.swift
//  data-scanner
//
//  Created by Ahmed Osman on 1/23/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import UIKit

enum HTTPStatus: Int, Error {
    
    // Informational - 1xx
    case `continue` = 100
    case switchingProtocols = 101
    case processing = 102

    // Success - 2xx
    case ok = 200
    case created = 201
    case accepted = 202
    case nonAuthoritativeInformation = 203
    case noContent = 204
    case resetContent = 205
    case partialContent = 206
    case multiStatus = 207
    case alreadyReported = 208
    case IMUsed = 226

    // Redirection - 3xx
    case multipleChoices = 300
    case movedPermanently = 301
    case found = 302
    case seeOther = 303
    case notModified = 304
    case useProxy = 305
    case switchProxy = 306
    case temporaryRedirect = 307
    case permenantRedirect = 308

    // Client Error - 4xx
    case badRequest = 400
    case unauthorized = 401
    case paymentRequired = 402
    case forbidden = 403
    case notFound = 404
    case methodNotAllowed = 405
    case notAcceptable = 406
    case proxyAuthenticationRequired = 407
    case requestTimeout = 408
    case conflict = 409
    case gone = 410
    case lengthRequired = 411
    case preconditionFailed = 412
    case payloadTooLarge = 413
    case URITooLong = 414
    case unsupportedMediaType = 415
    case rangeNotSatisfiable = 416
    case expectationFailed = 417
    case teapot = 418
    case misdirectedRequest = 421
    case unprocessableEntity = 422
    case locked = 423
    case failedDependency = 424
    case upgradeRequired = 426
    case preconditionRequired = 428
    case tooManyRequests = 429
    case requestHeaderFieldsTooLarge = 431
    case noResponse = 444
    case unavailableForLegalReasons = 451
    case SSLCertificateError = 495
    case SSLCertificateRequired = 496
    case HTTPRequestSentToHTTPSPort = 497
    case clientClosedRequest = 499

    // Server Error - 5xx
    case internalServerError = 500
    case notImplemented = 501
    case badGateway = 502
    case serviceUnavailable = 503
    case gatewayTimeout = 504
    case HTTPVersionNotSupported = 505
    case variantAlsoNegotiates = 506
    case insufficientStorage = 507
    case loopDetected = 508
    case notExtended = 510
    case networkAuthenticationRequired = 511
    
    /// The class (or group) which the status code belongs to.
    var responseType: ResponseType {
        switch self.rawValue {
        case 100..<200:
            return .informational(code: self.rawValue)
        case 200..<300:
            return .success(code: self.rawValue)
        case 300..<400:
            return .redirection(code: self.rawValue)
        case 400..<500:
            return .clientError(code: self.rawValue)
        case 500..<600:
            return .serverError(code: self.rawValue)
        default:
            return .undefined(code: self.rawValue)
        }
    }
}

/// The response class representation of status codes, these get grouped by their first digit.
enum ResponseType {
    
    /// - informational: This class of status code indicates a provisional response, consisting only of the Status-Line and optional headers, and is terminated by an empty line.
    case informational(code:Int)
    
    /// - success: This class of status codes indicates the action requested by the client was received, understood, accepted, and processed successfully.
    case success(code:Int)
    
    /// - redirection: This class of status code indicates the client must take additional action to complete the request.
    case redirection(code:Int)
    
    /// - clientError: This class of status code is intended for situations in which the client seems to have erred.
    case clientError(code:Int)
    
    /// - serverError: This class of status code indicates the server failed to fulfill an apparently valid request.
    case serverError(code:Int)
    
    /// - undefined: The class of the status code cannot be resolved.
    case undefined(code:Int)
}

extension ResponseType: BaseErrorProtocol {
    var message:String{
        switch self {
        case .serverError:
            return K.NetworkError.HTTPStatusErrorMessages.serverError
        case .clientError:
            return K.NetworkError.HTTPStatusErrorMessages.clientError
        default:
            return K.NetworkError.Error.defaultMessage
        }
    }
    
    var title:String{
        switch self {
        case .serverError:
            return K.NetworkError.HTTPStatusErrorTitles.serverError
        case .clientError:
            return K.NetworkError.HTTPStatusErrorTitles.clientError
        default:
            return K.NetworkError.Error.defaultTitle
        }
    }
    
    var image:String{
        switch self {
        case .serverError:
            return K.NetworkError.HTTPStatusErrorImages.serverError
        case .clientError:
            return K.NetworkError.HTTPStatusErrorImages.clientError
        default:
            return K.NetworkError.Error.defaultImage
        }
    }
}
