//
//  BaseErrorProtocol.swift
//  demos
//
//  Created by Ahmed Osman on 6/8/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation

protocol BaseErrorProtocol {    
    var message:String { get }
    var title:String { get }
    var image:String { get }
}
