//
//  ErrorHandler.swift
//  data-scanner
//
//  Created by Ahmed Osman on 2/6/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import Foundation
import Moya
//import Result

protocol ErrorDispatcherProtocol {
    func handleError<T>(error: Error, response: Response, errorType: T) -> NSError? where T:BaseErrorProtocol
    func handleError(error: Error, response: Response) -> NSError?
    func handleError(error: Error) -> NSError
}

protocol APIErrorProtocol {
    var message: String { set get }
}

class ErrorDispatcher: ErrorDispatcherProtocol{
    
    func handleError<T>(error: Error, response: Response, errorType: T) -> NSError? where T:BaseErrorProtocol {
        return nil
    }
    
    func handleError(error: Error, response: Response) -> NSError? {
//        let errorType = HTTPStatus.init(rawValue: response.statusCode)?.responseType
//        handleError(error: error, response: response, errorType: APIError.self)
        if let apiErrors = try? response.map(APIErrors.self), let apiError = apiErrors.errors.first {
            let userInfo = ["title": apiError.title, "message": apiError.detail, "image": ""]
            let nserror = NSError(domain: (error as NSError).domain, code: response.statusCode, userInfo: userInfo as [String : Any])
            return nserror
        }else{
            let userInfo = ["title": "", "message": "", "image": ""]
            let nserror = NSError(domain: (error as NSError).domain, code: response.statusCode, userInfo: userInfo as [String : Any])
            return nserror
        }
    }
    
    func handleError(error: Error) -> NSError {
        let errorType = NetworkError.init(rawValue: (error as NSError).code)
        let userInfo = ["title": errorType?.title, "message": errorType?.message, "image": errorType?.image]
         let nserror = NSError(domain: (error as NSError).domain, code: (error as NSError).code, userInfo: userInfo as [String : Any])
        return nserror
    }
}

struct APIErrors: Codable {
    var errors: [APIError]
}

struct APIError: Codable {
    var status: Int?
    var title: String?
    var detail: String?
}
