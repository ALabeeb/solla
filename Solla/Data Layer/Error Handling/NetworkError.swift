//
//  NetworkError.swift
//  data-scanner
//
//  Created by Ahmed Osman on 2/6/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import Foundation

enum NetworkError: Int {
    case timeout = -1001
    case requestCancelled = -999
    case cannotFindHost = -1003
    case notConnectedToInternet = -1009
    case networkConnectionLost = -1005
    case cannotConnectToHost = -1004
    case userAuthenticationRequired = -1013
    case resourceUnavailable = -1008
    case badServerResponse = -1011
    case DNSLookupFailed = -1006
    case unknown = -1
}

extension NetworkError: BaseErrorProtocol {
    var message: String{
        switch self {
        case .timeout:
            return K.NetworkError.NetworkErrorMessages.timeout
        case .requestCancelled, .badServerResponse, .unknown:
            return K.NetworkError.NetworkErrorMessages.requestCancelled
        case .notConnectedToInternet, .cannotFindHost, .networkConnectionLost, .cannotConnectToHost,
             .DNSLookupFailed:
            return K.NetworkError.NetworkErrorMessages.notConnectedToInternet
        case .userAuthenticationRequired:
            return K.NetworkError.NetworkErrorMessages.userAuthenticationRequired
        case .resourceUnavailable:
            return K.NetworkError.NetworkErrorMessages.resourceUnavailable
        }
    }
    
    var title: String{
        switch self {
        case .timeout:
            return K.NetworkError.NetworkErrorMessages.timeout
        case .requestCancelled, .badServerResponse, .unknown:
            return K.NetworkError.NetworkErrorMessages.requestCancelled
        case .notConnectedToInternet, .cannotFindHost, .networkConnectionLost, .cannotConnectToHost,
             .DNSLookupFailed:
            return K.NetworkError.NetworkErrorMessages.notConnectedToInternet
        case .userAuthenticationRequired:
            return K.NetworkError.NetworkErrorMessages.userAuthenticationRequired
        case .resourceUnavailable:
            return K.NetworkError.NetworkErrorMessages.resourceUnavailable
        }
    }
    
    var image: String{
        switch self {
        case .timeout:
            return K.NetworkError.NetworkErrorMessages.timeout
        case .requestCancelled, .badServerResponse, .unknown:
            return K.NetworkError.NetworkErrorMessages.requestCancelled
        case .notConnectedToInternet, .cannotFindHost, .networkConnectionLost, .cannotConnectToHost,
             .DNSLookupFailed:
            return K.NetworkError.NetworkErrorMessages.notConnectedToInternet
        case .userAuthenticationRequired:
            return K.NetworkError.NetworkErrorMessages.userAuthenticationRequired
        case .resourceUnavailable:
            return K.NetworkError.NetworkErrorMessages.resourceUnavailable
        }
    }
}
