//
//  UserEndPoint.swift
//  Solla
//
//  Created by arabpas on 11/5/20.
//

import Foundation
import Moya

enum UserEndPoint: BaseTarget {
    
    
    case userData
    case addresses
    case orders
    case order(_ orderid:Int)
    case cart
    
    var cache: Bool {
        return false
    }
    
    var headers: [String : String]? {
        var headers:[String:String] = [:]
         headers["Accept"]  = "application/json"
        switch self {
        default:
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = "Bearer \(LocaleStore.shared.value(for: K.Auth.tokenKey ) ?? "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMDU2ZmEwYTQwODlkYjY1ZTc2YTNhNzMyZjc1YWFmMTQ5NDFiYjVmMjFlZmJiMzAwYWM3NGMzY2IxNzhmN2IyNTZiNWVmMTZlYTY3MzAzOTIiLCJpYXQiOjE2MDEzODcwODEsIm5iZiI6MTYwMTM4NzA4MSwiZXhwIjoxNjMyOTIzMDgxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.obKwy1k4xMAx5NVaJr6BqkxY7yhRNakp1Limv6GaP1hhH-FQesEmIoRV7xlvYtUo-QcQoqdCUARX7Mw1ut69N83q6_Ou-W0d-46E001vRR_MxbhzL7aIea4nac6BzXOZT3IwvIAIbjhiDRf3Z_HnYA7RdjDMJ1AWi1tUEny9xb4tLW1LxMDDx5PmzAgiwLS5KBno-smypYbvzeTs-piHXw8sCgFJw4Jy4e0aHKOTaPn5GFN9vSG3rjznvjdLXwlS8B0JFmxtzSLTst8ncs-BRpPsXAEjx1UzOlTaZ-9v-mG-4TYTlPUVl5L4UNpgEGbDX4TOU6JxUJBdIcMiYjxdwZ5PN2wqbuqKTaMEkfQiD-A6QLy1878x_kx3R6yIbyCdpMJuZIOOEQpD-Y9wkEtj9vbKDU7RDzq_6lp0jEDJysOVieqV9OiXxD5wnO67q87Ss5KIcOOs0TkeKBtXWGhGekWgwtZdOkeZ8owyP7Hb9KdgJvgdUp0pFnQbqb70jvUiSytaqDQddxSTzL_agL1VFxDB4_087je4jq4-1ugMwjhEvE4kW0g1eEXZofHtipYePkH3ZeaD7oszYs0vS-RunIwiMYyD4i-btvslL4ynTakJ2BP7dgkq2aITqulddgnPlhn67CzNr46sZmDFtXP-1jL6RqrBqkGPq27xQ8cgCBo")"
        }
        return headers
    }
    
    var parameters: [String : Any] {
        let parameters = [String: Any]()
        switch self {
        default:
            return parameters
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var path: String {
        switch self {
        case .userData:
           return "my_profile/1"
        case .addresses:
            return "addresses"
        case .orders:
            return "order_history"
        case .order(let id):
            return "order_history/\(id)"
        case .cart:
            return "cart/1"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters, encoding: parameterEncoding)
    }
}
