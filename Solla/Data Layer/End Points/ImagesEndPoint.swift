//
//  ActionsEndPoint.swift
//  dawar
//
//  Created by Ahmed Osman on 8/17/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation
import UIKit
import Moya

enum ImagesEndPoint: BaseTarget {
    
    case uploadImage(images: [UIImage])
    
    var cache: Bool {
        return false
    }
    
    var headers: [String : String]? {
        var headers:[String:String] = [:]
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Accept"] = "application/json"
        return headers
    }
    
    var parameters: [String : Any] {
        return ["images":images]
    }
    
    var path: String {
        return "/image"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var images: [MultipartFormData] {
        switch self {
        case .uploadImage(let images):
            return images.map({                
                return MultipartFormData(provider: .data($0.jpegData(compressionQuality: 0.5) ?? Data()), name: "images[]", fileName: "\($0.hashValue)", mimeType: "image/jpg")
            })
        }
    }
    
    var task: Task {
         return .uploadMultipart(images)
    }
}
