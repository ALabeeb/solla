//
//  LoginEndPOint.swift
//  dawar
//
//  Created by Ahmed Osman on 7/16/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation
import Moya

enum LoginEndPoint: BaseTarget {
    case login(number: String, password: String, type: String)
    case logout(type: String,token:String)
    case update(language: String)
    //case fcm(model: FCMModel)
    case version

    var cache: Bool {
        return false
    }
    
    var parameters: [String : Any] {
        var parameters = [String: Any]()
//        switch self {
//        case .login(let number, let password, let type):
//            let loginAttributes = LoginAttributes(mobile_email: number, password: password)
//            let loginData = LoginData(id: "", type: type, attributes: loginAttributes)
//            let loginBody = LoginBody(data: loginData)
//            parameters = encodeModel(model: loginBody)
//        case .logout(let type, let token):
//            let attributes = LogoutData.Attributes.init(token: token)
//            let logoutData = LogoutData(id: "", type: type, attributes: attributes)
//            let logoutBody = LogoutBody(data: logoutData)
//            parameters = encodeModel(model: logoutBody)
//        case .update(let language):
//            let attributes = UpdateLanguageAttributes(language: language)
//            let data = UpdateLanguageData(id: "", type: "user", attributes: attributes)
//            let updateLanguage = UpdateLanguage(data: data)
//            parameters = encodeModel(model: updateLanguage)
//        case .fcm(let model):
////            model.data?.attributes?.device_type = "IOS"
//            parameters = encodeModel(model: model)
//        case .version:
//            parameters["mobile_type"] = "adapt_ios"
//        }
        return parameters
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .version:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    var path: String {
        switch self {
        case .version:
            return "/users/current-version"
        case .login:
            return "/users/login"
        case .logout:
            return "/users/logout"
        case .update:
            return "/users/update-language"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .version:
            return .get
        case .login, .logout, .update:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .login:
            return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: ["mobile_type":"adapt"])
        default:
            return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }
}
