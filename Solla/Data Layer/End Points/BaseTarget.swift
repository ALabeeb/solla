//
//  BaseTarget.swift
//  mknsa
//
//  Created by Ahmed Osman on 3/6/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation
import Moya

protocol BaseTarget: TargetType, Hashable, AccessTokenAuthorizable {
    var cache: Bool{ get }
    var parameters: [String: Any] { get }
}

extension BaseTarget {
    var headers: [String : String]? {
        var headers:[String:String] = [:]
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        return headers
    }
    
    var baseURL: URL {
             return URL(string: "http://sollaa.com/api")!
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var isStub: Bool{
        return false
    }
    
    var stubName: String{
        return "\(method.rawValue.lowercased()):\(path.lowercased())".trimmingCharacters(in: .whitespaces)
    }
}

extension BaseTarget {
    func encode(parameters: [String: String]) -> [String: Any] {
        let json = try? JSONEncoder().encode(parameters)
        do {
            return try (JSONSerialization.jsonObject(with: json!, options: []) as? [String: Any])!
        } catch {
            print(error.localizedDescription)
            return [:]
        }
    }
    
    func encodeModel<T>(model: T) -> [String: Any]  where T : Encodable{
        let json = try? JSONEncoder().encode(model)
        do {
            return try (JSONSerialization.jsonObject(with: json!, options: []) as? [String: Any])!
        } catch {
            print(error.localizedDescription)
            return [:]
        }
    }
}
