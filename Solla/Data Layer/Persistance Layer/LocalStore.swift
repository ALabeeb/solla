//
//  LocalStore.swift
//  Solla
//
//  Created by Ahmad Labeeb on 11/4/20.
//

import Foundation

class LocaleStore {
    
    private let standard = UserDefaults.standard
    static let shared = LocaleStore()
    private static let chainKey = "com.kiteagency.dawar"
    
    private init() {
        
    }
    
    func save(value: String, key: String, isSecure: Bool = false) {
        
        standard.set(value, forKey: key)
        
    }
    
    func remove(key: String) {
        if let _ = standard.string(forKey: key) {
            standard.removeObject(forKey: key)
        }
    }
    
    func value(for key: String) -> String? {
        if let value = standard.string(forKey: key) {
            return value
        }
        return nil
    }
}

/* we can use repository pattern to change between stores
 1- user defaults
 2- key chain
 3- core data
 4- network call
*/
