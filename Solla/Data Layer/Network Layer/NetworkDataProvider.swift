//
//  Logger.swift
//  data-scanner
//
//  Created by Ahmed Osman on 1/23/19.
//  Copyright © 2019 Makani. All rights reserved.
//

import Foundation
import Moya
//import Result
import RxSwift

class NetworkDataProvider<T: Hashable> where T: TargetType  {
    typealias CompletionBlock = (Result<Moya.Response, MoyaError>) -> Void
    private var plugins: [PluginType] = []
    private let provider: MoyaProvider<T>
    private let backgroundName = "BackgroundTaskNetwork"
    private var cancellableReqests : [T:Cancellable] = [:]
    
    init(authPlugin: PluginType? = nil, loggerPlugin: PluginType? = nil, activityPlugin: PluginType? = nil) {
        if let authPlugin = authPlugin{
            plugins.append(authPlugin)
        }
        if let activityPlugin = activityPlugin{
            plugins.append(activityPlugin)
        }
        if let loggerPlugin = loggerPlugin{
            plugins.append(loggerPlugin)
        }
        provider = MoyaProvider<T>(plugins: plugins)
    }
    
    func fetch(request: T, completion:@escaping CompletionBlock) {
        let backgroundTask = UIApplication.shared.beginBackgroundTask(withName: backgroundName) {
            print("Background Task Ended")
        }
        let cancelableRequest = provider.request(request) { (result) in
            print(result)
            self.cancellableReqests.removeValue(forKey: request)
            UIApplication.shared.endBackgroundTask(backgroundTask)
            completion(result)
        }
        cancellableReqests[request] = cancelableRequest
    }
    
    func fetch(request: T) -> Observable<Response> {
        return provider.rx.request(request).asObservable()
    }
    
    func cancelRequest(with name:T) -> Bool {
        if let request = cancellableReqests[name]{
            request.cancel()
            cancellableReqests.removeValue(forKey: name)
            return true
        }
        return false
    }
    
    func cancelAllRequest() {
        for request in cancellableReqests.values{
            request.cancel()
        }
        cancellableReqests.removeAll()
    }
    
    func isCancelled(name :T) -> Bool {
        if let request = cancellableReqests[name]{
            return request.isCancelled
        }
        return false
    }
}


enum LogEvent: String {
    case e = "‼️" // error
    case i = "ℹ️" // info
    case d = "💬" // debug
    case v = "🔬" // verbose
    case w = "⚠️" // warning
    case s = "🔥" // severe
}
