//
//  ActivityLoaderPlugin.swift
//  dawar
//
//  Created by Ahmed Osman on 8/18/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import UIKit

class ActivityIndicator {
    
    fileprivate let _lock = NSRecursiveLock()
    fileprivate var _count = 0
    static let activity = ActivityIndicator()
    let indicator: UIActivityIndicatorView = {
        let ind = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        ind.style = UIActivityIndicatorView.Style.gray        
        ind.hidesWhenStopped = true
        return ind
    }()
    
    private init() {
        
    }
    
    func showLoader() {
        if _count > 0 {
            increment()
            return
        }else{
            // show
            UIApplication.shared.keyWindow?.addSubview(indicator)
            indicator.center = UIApplication.shared.keyWindow?.center ?? CGPoint(x: 50, y: 50)
            UIApplication.shared.keyWindow?.bringSubviewToFront(indicator)
//            UIApplication.shared.beginIgnoringInteractionEvents()
            indicator.startAnimating()
        }
    }
    
    func hideLoader() {
        if _count > 0 {
            return
        }else{
            // hide
            indicator.stopAnimating()
//            UIApplication.shared.endIgnoringInteractionEvents()
        }
        decrement()
    }
    
    fileprivate func increment() {
        _lock.lock()
        _count = _count + 1
        _lock.unlock()
    }
    
    fileprivate func decrement() {
        _lock.lock()
        _count = _count - 1
        _lock.unlock()
    }
}
