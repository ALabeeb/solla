//
//  NetworkHandler.swift
//  eventupia
//
//  Created by Ahmed Osman on 6/18/19.
//  Copyright © 2019 Ahmed Osman. All rights reserved.
//

import Foundation
import Moya
//import Result
import RxSwift




class NetworkModel<T: TargetType> {
    private var provider = MoyaProvider<T>()
    
    func fetchData(request: T) -> Observable<Response> {
        return provider.rx.request(request).asObservable()
    }
    
    func fetch<M: Codable>(request: T) -> Observable<M> {
        return fetchData(request: request).map(M.self)
        
    }
}





class NetworkInteractor<T: Hashable> where T: TargetType {
    
    typealias CompletionResult = (data: Response?, error: Error?)
    typealias CompletionBlock = (CompletionResult) -> Void
    typealias BatchResponse = ([T:Any]) -> Void
    lazy var activityPlugin = NetworkActivityPlugin { (type, _) in
        switch type{
        case .began:
            ActivityIndicator.activity.showLoader()
        case .ended:
            ActivityIndicator.activity.hideLoader()
        }
    }
    private let network = NetworkDataProvider<T>(authPlugin: AccessTokenPlugin(tokenClosure: { _ in return  "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMDU2ZmEwYTQwODlkYjY1ZTc2YTNhNzMyZjc1YWFmMTQ5NDFiYjVmMjFlZmJiMzAwYWM3NGMzY2IxNzhmN2IyNTZiNWVmMTZlYTY3MzAzOTIiLCJpYXQiOjE2MDEzODcwODEsIm5iZiI6MTYwMTM4NzA4MSwiZXhwIjoxNjMyOTIzMDgxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.obKwy1k4xMAx5NVaJr6BqkxY7yhRNakp1Limv6GaP1hhH-FQesEmIoRV7xlvYtUo-QcQoqdCUARX7Mw1ut69N83q6_Ou-W0d-46E001vRR_MxbhzL7aIea4nac6BzXOZT3IwvIAIbjhiDRf3Z_HnYA7RdjDMJ1AWi1tUEny9xb4tLW1LxMDDx5PmzAgiwLS5KBno-smypYbvzeTs-piHXw8sCgFJw4Jy4e0aHKOTaPn5GFN9vSG3rjznvjdLXwlS8B0JFmxtzSLTst8ncs-BRpPsXAEjx1UzOlTaZ-9v-mG-4TYTlPUVl5L4UNpgEGbDX4TOU6JxUJBdIcMiYjxdwZ5PN2wqbuqKTaMEkfQiD-A6QLy1878x_kx3R6yIbyCdpMJuZIOOEQpD-Y9wkEtj9vbKDU7RDzq_6lp0jEDJysOVieqV9OiXxD5wnO67q87Ss5KIcOOs0TkeKBtXWGhGekWgwtZdOkeZ8owyP7Hb9KdgJvgdUp0pFnQbqb70jvUiSytaqDQddxSTzL_agL1VFxDB4_087je4jq4-1ugMwjhEvE4kW0g1eEXZofHtipYePkH3ZeaD7oszYs0vS-RunIwiMYyD4i-btvslL4ynTakJ2BP7dgkq2aITqulddgnPlhn67CzNr46sZmDFtXP-1jL6RqrBqkGPq27xQ8cgCBo" }), loggerPlugin: NetworkLoggerPlugin())
    
    func fetch(request: T, completion: @escaping CompletionBlock) {
        network.fetch(request: request) { (result) in
            let value = self.handleResult(result: result)
            completion(value)
        }
    }
    
    
    func fetch(request: T) -> Observable<Response> {        
        return network.fetch(request: request).catchError({ (error) -> Observable<Response> in
            let errorDispatcher: ErrorDispatcherProtocol = ErrorDispatcher()
            let nserror = errorDispatcher.handleError(error: error)
            throw nserror
        }).map({ (response) -> Response in
            let value = self.handleResult(result: response)
            if let data = value.data {
                return data
            }else{
                throw value.error ?? NSError()
            }
        })
    }
    
    func upload(request: T) -> Observable<Response> {
        return fetch(request: request)
    }
    
    func cancelRequest(with name:T) -> Bool {
        return network.cancelRequest(with: name)
    }
    
    func cancelAllRequest() {
        network.cancelAllRequest()
    }
    
    func isCancelled(name: T) -> Bool {
        return network.isCancelled(name: name)
    }
    
    func batchParallel(requests : [T], completion: @escaping BatchResponse){
        let group = DispatchGroup()
        var batchResponse:[T:Any] = [:]
        for request in requests {
            group.enter()
            fetch(request: request, completion: { (response, error)  in
                batchResponse[request] = response
                group.leave()
            })
        }
        group.notify(queue: .main) {
            completion(batchResponse)
        }
    }
    
    func batchParallel(requests : [T]) -> [T:Observable<Response>] {
        var batchResponse:[T:Observable<Response>] = [:]
        for request in requests {
            batchResponse[request] = fetch(request: request)
        }
        return batchResponse
    }
    
    func mapResponse<R: Codable>(data: Data) -> R?{
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(R.self, from: data)
        } catch  {
            return nil
        }
    }
    
    private func handleResult(result: Response) -> CompletionResult {
        var nserror: NSError?
        var data: Moya.Response?
        do{
            data = try result.filterSuccessfulStatusCodes()
        }catch{
            let errorDispatcher = ErrorDispatcher()
            nserror = errorDispatcher.handleError(error: error, response: result)
        }
        return (data, nserror)
    }
    
    private func handleResult(result: Result<Moya.Response, MoyaError>) -> CompletionResult {
        
        var nserror: NSError?
        var data: Moya.Response?
        
        switch result {
        case .success(let response):
            do{
                data = try response.filterSuccessfulStatusCodes()
            }catch{
                let errorDispatcher: ErrorDispatcherProtocol = ErrorDispatcher()
                nserror = errorDispatcher.handleError(error: error, response: response)
            }
        case .failure(let error):
            let errorDispatcher: ErrorDispatcherProtocol = ErrorDispatcher()
            nserror = errorDispatcher.handleError(error: error)
        }
        return (data, nserror)
    }
}
